import keras
from keras.models import Sequential
from keras.models import Model, load_model
from keras.layers import Conv2D, Conv1D
from keras.layers import MaxPooling2D, MaxPooling1D
from keras.layers import BatchNormalization
from keras.layers import Activation
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import Concatenate
from keras.utils import plot_model

def make_arq7_model(name):
    
    xa_input = Input(shape=(12, 12, 5,), name='xal_'+name)
    xd_input = Input(shape=(20, 15), name='xdl_'+name)
    
    out_final = make_arq7_internal_model(name,xa_input, xd_input)

    model = Model(inputs=[xa_input,xd_input], outputs=out_final)
    
    return model


def make_arq7_internal_model(name,xa_input, xd_input):
    # XAL Phase

    ## First Stage
    out = Conv2D(filters=32, kernel_size=(3,3), strides=(1, 1),data_format="channels_last", name='conv2d_1_'+name)(xa_input)
    out = BatchNormalization(name='batchnorm_1_'+name)(out)
    out = Activation(activation='relu', name='activ_1_'+name)(out)
    out = MaxPooling2D(pool_size=(2, 2), strides=(2,2), name='maxpool2d_1_'+name)(out)

    ## Second Stage
    out = Conv2D(filters=64, kernel_size=(3,3), strides=(1, 1), name='conv2d_2_'+name)(out)
    out = BatchNormalization(name='batchnorm_2_'+name)(out)
    out = Activation(activation='relu', name='activ_2_'+name)(out)
    out = MaxPooling2D(pool_size=(2, 2), strides=(2,2), name='maxpool2d_2_'+name)(out)

    xa_out = Flatten(name='flatten_xal_'+name)(out)

    # XDL Phase
    
    ## First Stage
    out = Conv1D(filters=32, kernel_size=3, strides=1,data_format="channels_last", name='conv1d_1_'+name)(xd_input)
    out = BatchNormalization(name='batchnorm_3_'+name)(out)
    out = Activation(activation='relu', name='activ_3_'+name)(out)
    out = MaxPooling1D(pool_size=2, strides=2, name='maxpool1d_1_'+name)(out)

    ## Second Stage
    out = Conv1D(filters=64, kernel_size=3, strides=1,data_format="channels_last", name='conv1d_2_'+name)(out)
    out = BatchNormalization(name='batchnorm_4_'+name)(out)
    out = Activation(activation='relu', name='activ_4_'+name)(out)
    out = MaxPooling1D(pool_size=2, strides=2, name='maxpool1d_2_'+name)(out)

    xd_out = Flatten(name='flatten_xdl_'+name)(out)

    # Ensemble
    out = Concatenate(name='concatenate_'+name)([xa_out,xd_out])
    
    out = Dense(256,activation='relu', name='dense_1_'+name)(out)
    out = Dropout(rate=0.2, name='dropout_1_'+name)(out)
    out = Dense(64,activation='relu', name='dense_2_'+name)(out)
    out = Dropout(rate=0.2, name='dropout_2_'+name)(out)
    out_final = Dense(1, name='out_'+name, activation='sigmoid')(out)

    return out_final


def make_arq7xal_model(name):
    xa_input = Input(shape=(12, 12, 5,), name='xal_'+name)
    out_final = make_arq7xal_internal_model(name,xa_input)
    model = Model(inputs=xa_input, outputs=out_final)
    return model


def make_arq7xal_internal_model(name,xa_input):
     # XAL Phase

    ## First Stage
    out = Conv2D(filters=32, kernel_size=(3,3), strides=(1, 1),data_format="channels_last", name='conv2d_1_'+name)(xa_input)
    out = BatchNormalization(name='batchnorm_1_'+name)(out)
    out = Activation(activation='relu', name='activ_1_'+name)(out)
    out = MaxPooling2D(pool_size=(2, 2), strides=(2,2), name='maxpool2d_1_'+name)(out)

    ## Second Stage
    out = Conv2D(filters=64, kernel_size=(3,3), strides=(1, 1), name='conv2d_2_'+name)(out)
    out = BatchNormalization(name='batchnorm_2_'+name)(out)
    out = Activation(activation='relu', name='activ_2_'+name)(out)
    out = MaxPooling2D(pool_size=(2, 2), strides=(2,2), name='maxpool2d_2_'+name)(out)

    xa_out = Flatten(name='flatten_xal_'+name)(out)
    
    out = Dense(256,activation='relu', name='dense_1_'+name)(xa_out)
    out = Dropout(rate=0.2, name='dropout_1_'+name)(out)
    out = Dense(64,activation='relu', name='dense_2_'+name)(out)
    out = Dropout(rate=0.2, name='dropout_2_'+name)(out)
    out_final = Dense(1, name='out_'+name, activation='sigmoid')(out)

    return out_final


def make_arq7xdl_model(name):
    xd_input = Input(shape=(20, 15), name='xdl_'+name)
    out_final = make_arq7xdl_internal_model(name,xd_input)
    model = Model(inputs=xd_input, outputs=out_final)
    return model


def make_arq7xdl_internal_model(name,xd_input):
    # XDL Phase
    
    ## First Stage
    out = Conv1D(filters=32, kernel_size=3, strides=1,data_format="channels_last", name='conv1d_1_'+name)(xd_input)
    out = BatchNormalization(name='batchnorm_3_'+name)(out)
    out = Activation(activation='relu', name='activ_3_'+name)(out)
    out = MaxPooling1D(pool_size=2, strides=2, name='maxpool1d_1_'+name)(out)

    ## Second Stage
    out = Conv1D(filters=64, kernel_size=3, strides=1,data_format="channels_last", name='conv1d_2_'+name)(out)
    out = BatchNormalization(name='batchnorm_4_'+name)(out)
    out = Activation(activation='relu', name='activ_4_'+name)(out)
    out = MaxPooling1D(pool_size=2, strides=2, name='maxpool1d_2_'+name)(out)

    xd_out = Flatten(name='flatten_xdl_'+name)(out)
    
    out = Dense(256,activation='relu', name='dense_1_'+name)(xd_out)
    out = Dropout(rate=0.2, name='dropout_1_'+name)(out)
    out = Dense(64,activation='relu', name='dense_2_'+name)(out)
    out = Dropout(rate=0.2, name='dropout_2_'+name)(out)
    out_final = Dense(1, name='out_'+name, activation='sigmoid')(out)

    return out_final