import Bio.PDB
import gzip
import math
import numpy as np

def degrees(rad_angle) :
    """Converts any angle in radians to degrees.

    If the input is None, the it returns None.
    For numerical input, the output is mapped to [-180,180]
    """
    if rad_angle is None :
        return None
    angle = rad_angle * 180 / math.pi
    while angle > 180 :
        angle = angle - 360
    while angle < -180 :
        angle = angle + 360
    return angle

def euclidian_distance(v, u):
    return np.sqrt(np.sum((v - u) ** 2))

def get_res_index(resname):
    standard_aa_names = {
        "GLY": 0, "ALA": 0, "PRO": 0, "VAL": 0, "LEU": 0, "ILE": 0, "MET": 0, # Aliphatic
        "PHE": 1, "TYR": 1, "TRP": 1, # Aromatic
        "SER": 2, "THR": 2, "CYS": 2, "ASN": 2, "GLN": 2, # Polar
        "LYS": 3, "ARG": 3, "HIS": 3, # Positive
        "ASP": 4, "GLU": 4 # Negative
    }
    
    if resname in standard_aa_names:
        return standard_aa_names[resname]
    else:
        return -1

def get_bin_index(value, bins):
    """
    bins = array(n+1)
    return index of n bins
    """
    for i in range(len(bins)):
        if i==0:
            continue
        if value<bins[i]:
            return i-1
    return len(bins)-2

def generate_XAL_reg(chain):
    # ramach[phi_bin_index, psi_bin_index, amino_acid_index]
    GRID_SZ = 12
    CHANNELS = 5
    ramach = np.zeros((GRID_SZ,GRID_SZ,CHANNELS))
    bins = np.linspace(-180,180,GRID_SZ+1)

    polypeptides = Bio.PDB.CaPPBuilder().build_peptides(chain)
    for poly_index, poly in enumerate(polypeptides) :
        phi_psi = poly.get_phi_psi_list()
        for res_index, residue in enumerate(poly) :
            phi, psi = phi_psi[res_index]

            if phi and psi :
                amino_acid_index = get_res_index(residue.resname)
                if amino_acid_index < 0:
                    continue
                phi_bin_index = get_bin_index(degrees(phi),bins)
                psi_bin_index = get_bin_index(degrees(psi),bins)

                ramach[phi_bin_index, psi_bin_index, amino_acid_index]+=1.0
                   
    return ramach

def generate_XD_reg(chain):    
    # distd[amino_acid_index_1, amino_acid_index_2, dist_index]
    CHANNELS = 5
    distd = np.zeros((CHANNELS,CHANNELS,20))
    bins = np.linspace(0, 150, 21)
    
    polypeptides = Bio.PDB.CaPPBuilder().build_peptides(chain)
    for poly_index, poly in enumerate(polypeptides) :
        ca_list = poly.get_ca_list()
        for res_index1 in range(len(poly)):
            for res_index2 in range(res_index1+1,len(poly)):
                u = ca_list[res_index1].get_coord()
                v = ca_list[res_index2].get_coord()
                d = euclidian_distance(u,v)  
                amino_acid_index_1 = get_res_index(poly[res_index1].resname)
                amino_acid_index_2 = get_res_index(poly[res_index2].resname)
                if (amino_acid_index_1 < 0) or (amino_acid_index_2 < 0):
                    continue
                dist_index = get_bin_index(d,bins)
                distd[amino_acid_index_1, amino_acid_index_2, dist_index]+=1.0
                distd[amino_acid_index_2, amino_acid_index_1, dist_index]+=1.0
    
    return distd

def translate_XD_XDL(xd):
    CHANNELS = 5
    xdl = []
    for i in range(CHANNELS):
        for j in range(i,CHANNELS):  
            xdl.append(xd[i,j,:])
    return np.array(xdl).T

def generate_XDL_reg(chain):
    CHANNELS = 5
    xdl = []
    xd = generate_XD_reg(chain)
    for i in range(CHANNELS):
        for j in range(i,CHANNELS):  
            xdl.append(xd[i,j,:])
    return np.array(xdl).T
